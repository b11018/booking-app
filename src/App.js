import { useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Login from './pages/Login';
import './App.css';
import { UserProvider } from './UserContext';
// ACTIVITY s53
import NotFound from './pages/Error';


function App() {

	const [user,setUser] = useState({
		_id: null,
		isAdmin: null
		// REPLACED BY ID AND ISADMIN
		// email: localStorage.getItem('email')
	})

	const unsetUser = () => {
		localStorage.clear();
	}

	useEffect(() => {
		fetch('http://localhost:4000/users/getUserDetails', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// captured the data of whomever is logged in
			console.log(data)

			// sets the user states values with the user details upon succesful login
			if(typeof data._id !== "undefined") {
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			// sets back the initial state of the user
			} else {
				setUser({
					id: null,
					isAdmin: null
				})
			}
		})

	}, [])

    return (
    	<UserProvider value={{user, setUser, unsetUser}}>
		    <Router>
		        <AppNavBar/>
		        <Container>
		        	<Routes>
			        	<Route exact path="/" element={<Home/>}/>
			        	<Route exact path="/courses" element={<Courses/>}/>
			        	<Route exact path="/courseView/:courseId" element={<CourseView/>}/>
			        	<Route exact path="/register" element={<Register/>}/>
			        	<Route exact path="/login" element={<Login/>}/>
			        	<Route exact path="/logout" element={<Logout/>}/>
			        	{/*ACTIVITY*/}
			        	<Route path="*" element={<NotFound />} />

		        	</Routes>
		        </Container>
		    </Router>
	    </UserProvider>
    );
}

export default App;