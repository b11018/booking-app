// import { useState, useEffect } from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// ACTIVITY
// MY SOLUTION
/*
export default function CourseCard(){
	return(
		<Row className="mt-3 mb-3">
			<Col>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h5>Sample Course</h5>
						</Card.Title>
						<Card.Text>
							<span className="boldText">Description:</span><br/>
							<span>This is a sample course offering.</span><br/>
							<br/>
							<span className="boldText">Price:</span><br/>
							<span>Php 40,000.00</span>
						</Card.Text>
						<Button variant="primary">Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
*/

// INSTRUCTOR SOLUTION

export default function CourseCard({courseProp}) {
	console.log(courseProp);
	// result: coursesData[0] - wdc001
	// console.log(typeof courseProp);
	// result: object

	// object destructuring
	const {name, description, price, _id} = courseProp;
	// console.log(price);

	// Syntax: const [getter, setter] = useState(initialValueOfGetter)
	/*	REPLACED WHEN CONNECTED TO DATABASE
	const [count, setCount] = useState(0);
	const [seats, seatsLess] = useState(30);
	const [isOpen, setIsOpen] = useState(false);

	const enroll = () => {
		if (count < 30 && seats > 0){
			setCount(count + 1); 
			seatsLess(seats - 1);
		} else {
			alert(`No more seats`);
		};
	};

	useEffect(() => {
		if(seats === 0){
			setIsOpen(true);
		}
	}, [seats]) // bracket required for this to run any time any dependency(seats) value changes
	*/

    return (
        <Card className= "mb-4 col-md-4">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
            </Card.Body>
        </Card>
    )
}