import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';
import { useEffect, useState } from 'react';

export default function Courses(){
	// console.log(coursesData);
	// holds the mock database
	// console.log(coursesData[0]);
	// captured the first object in coursesData

	/*	REPLACED WITH DATABASE COURSES
	const courses = coursesData.map(course => {
		return(
			<CourseCard key = {course.id} courseProp = {course}/>
		)
	});
	*/

	const [courses, setCourses] =useState([])

	useEffect(() => {
		fetch('http://localhost:4000/courses')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} courseProp={course}/>
				)
			}))
		})
	}, [])

	return(
		<>
			<h1> Courses Available: </h1>
				{courses}
			{/*<CourseCard courseProp = {coursesData[0]}/>*/}
		</>
	)
}