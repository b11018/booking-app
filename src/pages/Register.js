import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function Register(){

	// ACTIVITY s54
	const {user, setUser} = useContext(UserContext);

	const history = useNavigate();

	// ADDED TO MATCH USERCONTROLLER REQUIREMENTS
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	// REPLACED WITH PASSWORD VERIFICATION
	// const [password1, setPassword1] = useState('');

	console.log(email);
	console.log(password);
	// console.log(password1);

	useEffect(() => {
		if(firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password])

	function registerUser(e){
		e.preventDefault();

		// ACTIVITY s54
		/*
		localStorage.setItem('email', email)
		setUser({
			email: localStorage.getItem('email')
		});
		alert(`${email} is now registered and logged in. Welcome!`);
		*/

		fetch('http://localhost:4000/users/checkEmailExists', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data){
					Swal.fire({
						title: 'Duplicate email found',
						icon: 'info',
						text: 'Please provide another email'
					})
				} else {
					fetch('http://localhost:4000/users/', {
						method: 'POST',
						headers: {
							'Content-Type' : 'application/json'
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password
						})
					})
					.then(res => res.json())
					.then(data => {
						console.log(data)

						if(data.email){
							Swal.fire({
								title: 'Registration successful',
								icon: 'success',
								text: 'Thank you for registering'
							})

							history("/login")
							
						} else {
							Swal.fire({
								title: 'Registration failed',
								icon: 'error',
								text: 'Something went wrong'
							})
						}
					})
				}
			})

		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword('');
	};

	return (
		// ACTIVITY s54
		(user.id !== null)
		?
		<Navigate to="/courses"/>
		:

		<>
		<h1>Register Here:</h1>
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group controlId="firstName">
				<Form.Label className="mt-2">First Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please enter your first name here"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label className="mt-2">Last Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please enter your last name here"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label className="mt-2">Mobile Number:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please enter your 11-digit mobile number here"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label className="mt-2">Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Please enter your email here"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label className="mt-2">Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Please enter your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

		{ isActive
			?
			<Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
				Register
			</Button>
			:
			<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
				Register
			</Button>
		}
		</Form>
		</>
	)
}