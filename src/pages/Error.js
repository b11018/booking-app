// ACTIVITY s53
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function notFound(){
	var link = <Link style={{textDecoration: 'none'}} to="/">homepage</Link>;
	return (
		<Row>
			<Col className = "p-5">
				<h1>Page Not Found</h1>
				<p>Go back to the {link}</p>
			</Col> 
		</Row>
	)
}