import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(props){
	console.log(props);

	const {user, setUser} = useContext(UserContext);

	// State hooks to store the value of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	function loginUser(e){
		// Prevents page redirection via form submission
		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)
				// SweetAlert
				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome Back to Booking App of 182!'
				})
			} else {
				// SweetAlert
				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Check email or password'
				})
			}
		})
		/*	REPLACED WITH TOKEN
		localStorage.setItem('email', email)
		setUser({
			email: localStorage.getItem('email')
		});
		*/
		// Clear input fields after submission
		// Setter works asynchronously
		setEmail('');
		setPassword('');
		// REPLACED BY SWEET ALERT
		// alert(`${email} is verified. Welcome back!`);
	};

	const retrieveUserDetails = (token) => {
		fetch('http://localhost:4000/users/getUserDetails', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				_id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	return (
		(user.id !== null)
		?
		<Navigate to="/courses"/>
		:

		<>
		<h1>Login</h1>
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label className="mt-2">Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Please input your email here"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label className="mt-2">Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Please input your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

		{ isActive
			?
			<Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-5">
				Submit
			</Button>
			:
			<Button variant="secondary" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
				Login
			</Button>
		}
		</Form>
		</>
	)
}